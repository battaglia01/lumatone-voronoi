#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  7 04:14:29 2021

@author: mike
"""
#----------------------------------------------
# Header
import mikelib
from mikelib.huge_import_list import *
# mikelib.check_spyder_import() # comment if just a script!
#----------------------------------------------

N = 20          # size of key region to plot
ZOOM = 3        # axis zoom
SIZE = 5.8

import matplotlib.pyplot as plt

# Useful function to set the size of an axis (not including the toolbars)
def set_size(ax, w, h):
    """ w, h: width, height in inches """

    l = ax.figure.subplotpars.left
    r = ax.figure.subplotpars.right
    t = ax.figure.subplotpars.top
    b = ax.figure.subplotpars.bottom
    figw = float(w)/(r-l)
    figh = float(h)/(t-b)
    ax.figure.set_size_inches(figw, figh)
matplotlib.axes.Axes.set_size = set_size

# The function to be called anytime a slider's value changes
def update(val):
    lumatone_voronoi(angle_slider.val, exp(stretch_slider.val))
    # this refreshes the slider but doesn't always seem to work
    # angle_slider.__init__(
    #     ax=axangle,
    #     label='angle',
    #     valmin=-90,
    #     valmax=90,
    #     valinit=angle_slider.val
    # )
    # stretch_slider.__init__(
    #     ax=axstretch,
    #     label="stretch",
    #     valmin=-2,
    #     valmax=2,
    #     valinit=stretch_slider.val,
    #     orientation="vertical"
    # )

global stretch_slider, angle_slider, main_axes
stretch_slider = None
angle_slider = None
axcolor = 'lightgoldenrodyellow'
main_axes = plt.axes(label="Main")
axangle = plt.axes([0.2, 0.02, 0.6, 0.03], facecolor=axcolor, label="Angle")
axstretch = plt.axes([0.03, 0.25, 0.0225, 0.63], facecolor=axcolor, label="Stretch")
@mikelib.make_cmd_line
def lumatone_voronoi(angle:float=0, stretch:float=1):
    main_axes.clear()
    global stretch_slider, angle_slider

    # this is the lumatone's angle relative to 0 = rightward
    deg1 = 16.10211377341572
    # deg1 = 0
    deg2 = deg1 - 60            # the down-right direction

    # h is our hexagonal matrix, r is our rotation, s is our scaling
    h = array([[cos(deg1*tau/360), sin(deg1*tau/360)],
                [cos(deg2*tau/360), sin(deg2*tau/360)]])
    r = array([[cos(angle*tau/360), -sin(angle*tau/360)],
               [sin(angle*tau/360), cos(angle*tau/360)]])
    s = diag([stretch, 1/stretch])

    # now convert to our stretched coordinate system and voronoi
    points = cartesian(np.r_[-N:N], np.r_[-N:N])
    mapped_points = points @ h @ r @ s
    vor = spatial.Voronoi(mapped_points)

    # now change stuff. we can't change vor.points directly, so we have to
    # do it this way
    vor.__dict__["points"] = vor.points @ inv(s) @ inv(r)
    vor.__dict__["_points"] = vor._points @ inv(s) @ inv(r)
    vor.__dict__["vertices"] = vor.vertices @ inv(s) @ inv(r)

    fig = spatial.voronoi_plot_2d(vor, show_vertices=False, ax=main_axes)
    main_axes.set_size(SIZE, SIZE)

    main_axes.set_xlim([-ZOOM, ZOOM])
    main_axes.set_ylim([-ZOOM, ZOOM])

    ######
    # Now add sliders

    # Make a horizontal slider to control the angle.
    if angle_slider is None:
        angle_slider = Slider(
            ax=axangle,
            label='angle',
            valmin=-90,
            valmax=90,
            valinit=angle,
        )
        angle_slider.on_changed(update)

    # Make a vertically oriented slider to control the stretch
    if stretch_slider is None:
        stretch_slider = Slider(
            ax=axstretch,
            label="stretch",
            valmin=-2,
            valmax=2,
            valinit=stretch,
            orientation="vertical"
        )
        stretch_slider.on_changed(update)


    plt.show()


#----------------------------------------------
# Footer
mikelib.callable_module.make_callable(__name__)
mikelib.make_cmd_line.run_if_cmd_line() # comment if just a script!
#----------------------------------------------
